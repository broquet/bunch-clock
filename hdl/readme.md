# HDL source code

## Structure
|Name  | Description |
|--|--|
|bd|Zynq PS7 block design|
|ip_cores|common dependencies. This typically git submodules of OHWR (general-cores, wr-cores...)|
|modules|application rtl modules|
|syn|synthesis folder|
|top|whole design wrapper|

## Block design
This project uses the Zynq SoC component. Thus, a block design is used for the PS7 and corresponding AXI interconnect instantiation.
It has been decided to export block design to the top instead of having block design as the top.
This allows continuing in non-project mode and keeping as much as possible the classic OHWR repo structure.

## Modify block design - HowTo
To edit the block design, the following procedure must be used:
- cd bd/
- make project
- make gui

This step assumes that Vivado design suite is in the path. The block design has been edited with Vivado 2019.1

After block design modifications and validation, do the following steps:
- right-click on the system.bd file (in sources tab) and select "Generate Output Products..."
- select "Global" synthesis option and click on "Generate"
- right-click on the system.bd file and select "Create HDL Wrapper..."
- click on "Run Synthesis" (in Flow Navigator)

Once synthesis is finished, close Vivado GUI, and type (in the hdl/bd/ directory):
- make export
- make clean

The "make export" command writes the block design configuration as tcl script which is sourced when created the project.
Then, it copies the block design DCP (Design Check Point) in the hdl/syn directory.
The DCP allows to consider the block design as a blackbox at the synthesis step, and it is linked at the begining of par step.
