# Software

## Overview
This directory contains all stuff necessary to compile and execute CITY
software:
- application: software running on embedded linux. This includes a simple basic
  library to interface FPGA and hardware peripherals, and basic tools provided
  to configure and manage the CITY board.
- Real-Time (RT) software: software running in LM32 CPU softcores synthesized in
  FPGA:
  - White Rabbit Node Core (WRNC): CPU (x3) inside hdl WR Node Core for RFoWR,
    event generator and event trigger.
  - WR Core: CPU (x1) inside WR core for WR PTP mechanism.


## Contents
| Item           | Description                                                 |
|----------------|-------------------------------------------------------------|
|app             | Application programs (tools & library).  |
|common          | Contains includes common to application and RT softwares.   |
|rt              | RT software dedicated to WRNC CPUs and WR PTP Core CPU.     |


## Step-by-step software compilation
All commands (identified by the '$' prefix character) are given relatively from
sw directory:  
\<repo\>/sw


### application
Export the ARM toolchain to cross compile software, for instance:
```
$ export CROSS_COMPILE=/opt/Vivado/2020.1/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-
```

Compile library firstly:  
```
$ cd app/lib
$ make build
```

Compile tools:  
```
$ cd app/tools
$ make build
```

### LM32 Real-Time softwares
For LM32 software compilation, you need to have a LM32 cross compilation
toolchain installation.  
There are toolchain downloads available here:  
32bits: https://www.ohwr.org/project/wr-cores/uploads/a2e8eeba448fbc8d580e68004e6f6c7f/lm32.tar.xz  
64bits: https://www.ohwr.org/project/wr-cores/uploads/2776ce0ba43503d1486ae205b48fb450/lm32_host_64bit.tar.xz

Export cross compilation toolchain, for instance:
```
$ export CROSS_COMPILE=/opt/lm32/bin/lm32-elf-
```

Compile the WR PTP Core software:  
```
$ cd rt/wrpc-sw
$ make city_defconfig
$ make
```

Compile the WRNC softwares:  
```
$ cd rt/wrnc/rfowr
$ make
$ cd rt/wrnc/evg
$ make
$ cd rt/wrnc/evt
$ make
```
